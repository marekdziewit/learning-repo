package com.test.marek.mobicasitetester;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class TestSite extends ActionBarActivity implements IsSiteOnlineChecker.OnlineCheckerHandler {
    private static final String LOG_TAG = TestSite.class.getSimpleName();

    @InjectView(R.id.editTextTypeUrlHere)
    EditText typeUrlHere;

    @InjectView(R.id.listViewActivityTestSite)
    ListView listView;

    ProgressDialog progressBar;

    ArrayList<SingleUrlItem> urls;
    private SiteTestAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_mobica_site);
        ButterKnife.inject(this);
        urls = new ArrayList<>();
        adapter = new SiteTestAdapter(this, urls);
        listView.setAdapter(adapter);
    }

    @OnClick(R.id.buttonCheckConWithMobica)
    public void onClick() {
        if (isNetworkConnected()) {
            String url = typeUrlHere.getText().toString();
            runDialog(); // running Fetching dialog for waiting until OnlineChecker done background task
            new IsSiteOnlineChecker(this).execute(url);
        } else
            Log.d(LOG_TAG, "No internet connection!");
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }

    public void runDialog() {
        progressBar = new ProgressDialog(TestSite.this, R.style.MyTheme);
        progressBar.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        progressBar.setCancelable(false);
        progressBar.show();
    }

    @Override
    public void result(boolean isSucceed) {
        resultMessage(isSucceed);
        closeDialog();
        addItemToListView(isSucceed);
    }

    public void closeDialog() {
        progressBar.dismiss();
    }

    public void resultMessage(boolean isConnected) {
        if (isConnected) {
            Toast.makeText(this, "SUCCES", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "FAIL", Toast.LENGTH_SHORT).show();
        }
    }

    private void addItemToListView(boolean isOK) {
        urls.add(new SingleUrlItem(typeUrlHere.getText().toString(), isOK));
//        listView.invalidateViews();
        adapter.notifyDataSetChanged();
    }


    static class SiteTestAdapter extends BindableAdapter<SingleUrlItem> {
        ArrayList<SingleUrlItem> urls;

        public SiteTestAdapter(Context context, ArrayList<SingleUrlItem> urlItems) {
            super(context);
            this.urls = urlItems;
        }

        @Override
        public int getCount() {
            return urls.size();
        }

        @Override
        public SingleUrlItem getItem(int position) {
            return urls.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        class ViewHolder {
            @InjectView(R.id.imageView_list_single_row)
            ImageView imageView;
            @InjectView(R.id.textView_list_single_row)
            TextView textView;

            public ViewHolder(View v) {
                ButterKnife.inject(this, v);
            }

            public void bind(SingleUrlItem item) {
                textView.setText(item.url);
                if (item.isOk)
                    imageView.setImageResource(R.drawable.yes_indicator);
                else
                    imageView.setImageResource(R.drawable.no_indicator);
            }
        }

        @Override
        public View newView(LayoutInflater inflater, int position, ViewGroup container) {
            View v = inflater.inflate(R.layout.site_tester_single_row, container, false);
            v.setTag(new ViewHolder(v));
            return v;
        }

        @Override
        public View bindView(SingleUrlItem item, int position, View view) {
            ViewHolder viewHolder = (ViewHolder) view.getTag();
            viewHolder.bind(item);
            return view;
        }
    }
}
