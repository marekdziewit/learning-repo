package com.test.marek.mobicasitetester;

/**
 * Created by marek on 06.02.15.
 *
 *
 */
public class SingleUrlItem {
    public String url;
    public boolean isOk;

    public SingleUrlItem(String url,boolean isOk){
        this.isOk = isOk;
        this.url = url;
    }
}
