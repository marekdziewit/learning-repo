package com.test.marek.mobicasitetester;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by marek on 03.02.15.
 *
 *
 */
public class IsSiteOnlineChecker extends AsyncTask<String, Void, Void> {
    private static final String TAG = "IsMobicaOnlineChcker";

    interface OnlineCheckerHandler {
        void result(boolean isSucceed);
    }

    private OnlineCheckerHandler handler;

    public IsSiteOnlineChecker(OnlineCheckerHandler handler) {
        super();
        this.handler = handler;
    } // Context from place where called.


    public boolean isOK = false;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(String... params) {
        String url = params[0];
        checkHeadResponse(url);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        handler.result(isOK);

    }

    void checkHeadResponse(String url) {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("HEAD");
            int responseCode = connection.getResponseCode();
            isOK = responseCode == 200;
        } catch (IOException ex) {
            Log.e(TAG, ex.getMessage());
            isOK = false;
        }
    }

}
